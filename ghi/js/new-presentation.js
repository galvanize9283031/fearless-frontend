document.addEventListener('DOMContentLoaded', async function () {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    const getId = (url) => {
        const comps = url.split('/');
        return comps[comps.length - 2];
    }
    if (response.ok) {
        const responseData = await response.json();
        const element = document.querySelector("#conference");
        // load options into the select element
        for (let conference of responseData.conferences) {
            const option = document.createElement("option");
            option.text = conference.name;
            option.value = getId(conference.href);
            element.add(option);
        }
    }
    // add an event listener for the submit event from the 'create-presentation-form' form.
    const form = document.querySelector("#create-presentation-form");
    form.addEventListener('submit', async (event) => {
        event.preventDefault();
        // do a POST request to the API
        const formData = new FormData(form);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conference = document.querySelector("#conference");
        const id = conference[conference.selectedIndex].value;
        const presentationUrl = `http://localhost:8000/api/conferences/${id}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            form.reset();
            const newPresentation = await response.json();
        }
    });
});
