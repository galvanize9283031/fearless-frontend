/**
 * This file contains the JavaScript code for the new location page.
 * It is used by the new-location.html page.
 */


/**
 * This function is called when the form is submitted.
 * It retrieves the data from the form and sends it to the API.
 */
document.addEventListener('DOMContentLoaded', async function () {
    const url = 'http://localhost:8000/api/states/';
    const response = await fetch(url);
    if (response.ok) {
        const responseData = await response.json();
        const element = document.querySelector("#state");
        // load options into the select element
        for (let state of responseData.states) {
            const option = document.createElement("option");
            option.text = state.name;
            option.value = state.abbreviation;
            element.add(option);
        }
    }
    // add an event listener for the submit event from the 'create-location-form' form.
    const form = document.querySelector("#create-location-form");
    form.addEventListener('submit', async (event) => {
        event.preventDefault();
        // do a POST request to the API
        const formData = new FormData(form);
        const json = JSON.stringify(Object.fromEntries(formData));
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            form.reset();
            const newLocation = await response.json();
        }
    });
});
